import renderer from 'react-test-renderer';
import { render, screen, cleanup} from '@testing-library/react'
import Card from '../Card'


afterEach(() => {
    cleanup()
})


test("Card for h1", () => {

    const data = {
        id : 1,
        name : "Hafis",
        isValid: true
    }
    render(<Card data={data}/>)

    const cardElement = screen.getByTestId(`data-test-${data.id}`)
    expect(cardElement).toBeInTheDocument()
    expect(cardElement).toContainHTML(`<h1>${data.name}</h1>`)
    // console.log(cardElement)
})


test("snapshot testing" , () => {
    const data = {
        id : 1,
        name : "Hafis",
        isValid: true
    }
    const tree = renderer.create(<Card data={data}/>)
    expect(tree).toMatchSnapshot()
})