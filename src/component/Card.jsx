

export default function Card({data}) {
    const {name, isValid } = data
    console.log(data, "<<<")

    const h1 = <h1>{name}</h1>
    const elementName = isValid ? h1 : <strike>{name}</strike>

    return (
        <div data-testid={`data-test-${data.id}`}>
            <h1>Hello world</h1>
            {elementName}
        </div>
    )


}