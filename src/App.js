import './App.css';
import Card from './component/Card'

function App() {
  const data = [
    {id : 1 , name : 'Hafis', isValid : true},
    {id : 1 , name : 'Anton', isValid : false}
  ]

  return (
    <div className="App">
      {
        data.map(el => 
          <Card data={el} />)
      }
    </div>
  );
}

export default App;
